#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include "jni_test.h"
#include "library.c"

/**
 * @author Francisco Guerra (francisco.guerra@ulpgc.es)
 * @version 1.0
 */

void testExercise_A() {
    // Given
    int value[] = {-4};

    // When
    int* amount = oddEvenExchangedValues(value, 1);

    // Then
    assertArrayEquals_int(value, 1, amount, 1);
}

void testExercise_B() {
	// Given
		    int value[] = {-4, -8};
		    int finalvalue[] = {-8, -4};

		    // When
		    int* amount = oddEvenExchangedValues(value, 2);

		    // Then
		    assertArrayEquals_int(finalvalue, 2, amount, 2);
		}

void testExercise_C() {
	// Given
		    int value[] = {-4, -8, -1};
		    int finalvalue[] = {-8, -4, -1};

		    // When
		    int* amount = oddEvenExchangedValues(value, 3);

		    // Then
		    assertArrayEquals_int(finalvalue, 3, amount, 3);
		}

void testExercise_D() {
	// Given
		    int value[] = {-4, -8, -1, 22};
		    int finalvalue[] = {-8, -4, 22, -1};

		    // When
		    int* amount = oddEvenExchangedValues(value, 4);

		    // Then
		    assertArrayEquals_int(finalvalue, 4, amount, 4);
		}

void testExercise_E() {
	// Given
	    int value[] = {-4, -8, -1, 22, 67};
	    int finalvalue[] = {-8, -4, 22, -1, 67};

	    // When
	    int* amount = oddEvenExchangedValues(value, 5);

	    // Then
	    assertArrayEquals_int(finalvalue, 5, amount, 5);
	}

