#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>

/**
 * @author Francisco Guerra (francisco.guerra@ulpgc.es)
 * @version 1.0
 */

int* oddEvenExchangedValues(int* value, int valueLength) {
    if(valueLength == 1){
        return value;
    }

    for(int i = 0; i<valueLength-1; i=i+2){
        int devolver = value[i];
        value[i] = value[i+1];
        value[i+1] = devolver;
    }

    return value;
}
