#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#include "jni_inout.h"
#include "library.c"
extern JNIEnv *javaEnv;

/**
 * @author Francisco Guerra (francisco.guerra@ulpgc.es)
 * @version 1.0
 */

JNIEXPORT jobjectArray JNICALL Java_library_oddEvenExchangedValues_1
  (JNIEnv *env, jobject object, jobjectArray value, jint valueLength)
{
    javaEnv = env;
    int c_valueLength = toInt(valueLength);
    const char** c_value = toIntArray(value, c_valueLength);
    const char** c_outValue = oddEvenExchangedValues(c_value, c_valueLength);
    return toJintArray(c_outValue, &c_valueLength);
}

